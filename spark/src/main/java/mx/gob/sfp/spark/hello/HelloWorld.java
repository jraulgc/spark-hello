package mx.gob.sfp.spark.hello;

import static spark.Spark.*;

public class HelloWorld {
	
	public static void main(String... a) {
		
		get("/hello", (req, res) -> "Hello Francisco");
	}

}
